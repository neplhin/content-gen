const telegraph = require('telegraph-node')
const ph = new telegraph()
const fs = require('fs');

const init = async function () {
    ph.createAccount('OpenKiev', {short_name: 'OpenKiev', author_name: 'Open Kiev'})
        .then(result => {
            fs.writeFileSync('./data/telegraphCredentials.json', JSON.stringify(result, null, 4));
        })
}

module.exports = init
