const telegraph = require('telegraph-node')
const ph = new telegraph()

const create = async function (token, title, content) {
    return ph.createPage(token, title, content, null)
        .then(result => {
            return result.url
        })
        .catch(error => {
            console.log(error)
        })
}

module.exports = create