const telegraphCreate = require('../../services/telegra.ph/telegraphCreate')
const credentials = require('../../services/telegra.ph/telegraphCredentials')
const cheerio = require('cheerio')
const rp = require('request-promise')

const getPost = async function () {
  const URL = 'https://gloss.ua/afisha_arts'
  const results = []

  return rp(URL)
    .then(res => {
      const $ = cheerio.load(res)

      const firstPostUrl = $('.swiper-slide').find('a').attr('href')
      const firstPostTitle = $('.swiper-slide').find('h3').text()

      if (firstPostUrl !== '' && firstPostTitle !== '') {
        results.push({
          title: firstPostTitle,
          url: firstPostUrl
        })
      }

      $('.card').each((i, el) => {
        const postTitle = $(el).find('h4').text()
        const postUrl = $(el).find('a').attr('href')

        if (postTitle !== '' && postUrl !== '') {
          results.push({
            title: postTitle,
            url: postUrl
          })
        }
      })
      return createPost(results[0].url).then(res => {
        return res
      })
    })
    .catch(err => {
      console.log(err)
    })
}

async function createPost (url) {
  return rp(url)
    .then(res => {
      const $ = cheerio.load(res, { decodeEntities: false })
      const elements = $('.article *')
      const title = $('.topic-header').length === 0 ? $('.card-title').first().text() : $('.topic-header').first().text()


      if (elements.length < 50) {
          return createSimplePost(elements, $, title).then(res => {
              return res
          })
      } else {
        return createCompilationPost(elements, $, title).then(res => {
          return res
        })
      }
    })
    .catch(err => {
      console.log(err)
    })
}

async function createSimplePost (elements, $, title) {
    const result = [];
    result.push(title.concat('\n\n'));

    elements.map((i, el) => {
        const elem = elements[Object.keys(elements)[i]];
        if (elem.name === 'p') {
            result.push($(elem).text().concat('\n\n'))
        }
    });
    result.pop()
    return result.join('')
}

async function createCompilationPost (elements, $, title) {
  const contentParse = [];
  const result = [];
  result.push(title.concat('\n\n'));

  elements.map((i, el) => {
    const elem = elements[Object.keys(elements)[i]]
        if (elem.name === 'img') {

            // Fix url for original image. By Alex.
            let imgUrl = $(elem).attr('src')
            let clearedUrl = ""
            if(imgUrl.includes("100x") && imgUrl.includes("/t/") ) {
              clearedUrl = imgUrl.slice(0 , imgUrl.lastIndexOf("_")).replace("/t/" , "/") +
              imgUrl.slice(imgUrl.lastIndexOf(".") , imgUrl.length);
            } else {
              clearedUrl = imgUrl;
            }

            contentParse.push({
                tag: 'img',
                attrs: {src: clearedUrl}
            })
        }
        if (elem.name === 'h2') {
            result.push(`- ${$(elem).text()}\n`);

            contentParse.push({
                tag: 'a',
                attrs: {href: $(elem).find('a').attr('href')},
                children: [{
                    tag: 'h3',
                    children: [$(elem).text()]
                }]
            })
        }
        if (elem.name === 'p') {
            if ($(elem).find('strong').length === 1) {
                contentParse.push({
                    tag: 'p',
                    children: [{
                        tag: 'strong',
                        children: [$(elem).text()]
                    }]
                })
            } else {
                contentParse.push({
                    tag: 'p',
                    children: [$(elem).text()]
                })
            }
        }
    });
    contentParse.shift()
    contentParse.pop()

    return telegraphCreate(credentials.access_token, title, contentParse, {
      return_content: true
    }).then(res => {
        result.push(`\n ${res}`)
        return result.join('')
    })
}

module.exports = getPost
